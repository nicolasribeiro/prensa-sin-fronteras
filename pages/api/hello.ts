import type { NextApiRequest, NextApiResponse } from "next";

type Data = { msg: string };

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  switch (req.method) {
    case "GET":
      return getHelloWorld(req, res);

    //  case 'POST':
    //    return postFunction(req, res);

    //  case 'PUT':
    //    return putFunction(req, res);

    //  case 'DELETE':
    //    return deleteFunction(req, res);

    default:
      return res.status(400).json({ msg: "Bad Request" });
  }
}

const getHelloWorld = async (
  req: NextApiRequest,
  res: NextApiResponse<Data>
) => {
  throw new Error("Function not implemented.");
};
